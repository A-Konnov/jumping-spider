﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

[CreateAssetMenu]
public class SOChunkBD : ScriptableObject
{
    [SerializeField] private LevelChunk m_chunkStartPrefab;
    [SerializeField] private LevelChunk m_chunkPrefab;
    [SerializeField] private int m_countPull = 5;
    private List<LevelChunk> m_chunkPull;

    public int GetCountPull { get => m_countPull; }
    public List<LevelChunk> GetChunkPull { get => m_chunkPull; }

    public void GeneratePull()
    {
        m_chunkPull = new List<LevelChunk>();
        for (int i = 0; i < m_countPull; i++)
        {
            AddChunk();            
        }
    }

    private LevelChunk AddChunk()
    {
        var chunk = Instantiate(m_chunkPrefab);
        chunk.GetComponent<TilemapRenderer>().enabled = false;
        chunk.gameObject.SetActive(false);
        m_chunkPull.Add(chunk);
        return chunk;
    }

    public LevelChunk GetChunk()
    {
        foreach (var chunk in m_chunkPull)
        {
            if (chunk.gameObject.activeSelf == false)
            {
                chunk.gameObject.SetActive(true);
                return chunk;
            }
        }

        var newChunk = AddChunk();
        newChunk.gameObject.SetActive(true);
        return newChunk;
    }

    public LevelChunk GetStartChunk()
    {
        var startChunk = Instantiate(m_chunkStartPrefab);
        startChunk.GetComponent<TilemapRenderer>().enabled = false;
        startChunk.gameObject.SetActive(true);
        return startChunk;
    }


}
