﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class SOBranchDB : ScriptableObject
{
    [SerializeField] private GameObject[] m_branchPrefabs;
    [SerializeField] private int m_countBranchPull = 10;
    private List<GameObject[]> m_branchs;

    public void CreatePull()
    {
        m_branchs = new List<GameObject[]>();
        for (int versionBranch = 0; versionBranch < m_branchPrefabs.Length; versionBranch++)
        {
            GameObject[] branchs = new GameObject[m_countBranchPull];

            for (int i = 0; i < branchs.Length; i++)
            {
                branchs[i] = Instantiate(m_branchPrefabs[versionBranch]);
                branchs[i].SetActive(false);
            }

            m_branchs.Add(branchs);
        }
    }

    public GameObject GetRandomBranch()
    {
        int randomVersionBranch = UnityEngine.Random.Range(0, m_branchPrefabs.Length);

        var branchs = m_branchs[randomVersionBranch];
        foreach (var branch in branchs)
        {
            if (branch.activeSelf == false)
            {        
                return RotateBranch(branch);
            }
        }

        return null;
    }

    private GameObject RotateBranch(GameObject branch)
    {
        int randomRotate = UnityEngine.Random.Range(0, 2);
        if (randomRotate == 0)
        {
            branch.transform.rotation = Quaternion.Euler(0, 0, 0);
        }
        else
        {
            branch.transform.rotation = Quaternion.Euler(0, 180, 0);
        }
        return branch;
    }
}
