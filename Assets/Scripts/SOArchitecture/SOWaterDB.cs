﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class SOWaterDB : ScriptableObject
{
    [SerializeField] private GameObject m_prefabWater;
    [SerializeField] private int m_maxWaterOnChunk = 5;
    public int GetMaxWaterOnChunk { get => m_maxWaterOnChunk; }

    [SerializeField] private int m_countWaters = 10;
    private List<GameObject> m_waters;

    public void CreatePull()
    {
        m_waters = new List<GameObject>();
        for (int i = 0; i < m_countWaters; i++)
        {
            AddWaterInPull();
        }
    }

    public GameObject GetWater()
    {
        while (true)
        {
            foreach (var water in m_waters)
            {
                if (!water.activeSelf)
                {
                    water.SetActive(true);
                    return water;
                }
            }

            AddWaterInPull();
        }
    }

    private void AddWaterInPull()
    {
        var water = Instantiate(m_prefabWater);
        water.SetActive(false);
        m_waters.Add(water);
    }

    

}
