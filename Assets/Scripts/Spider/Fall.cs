﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fall : MonoBehaviour
{
    [SerializeField] private GameObject m_camera;
    private Tracking m_tracking;

    private Vector3 m_firstPosition;
    private Vector3 m_firstPositionCamera;
    private Jump m_jump;
    private WebStart m_webStart;
    private Collider2D m_collider;

    private void Start()
    {
        m_jump = GetComponent<Jump>();
        m_webStart = GetComponent<WebStart>();
        m_collider = GetComponent<Collider2D>();
        m_tracking = m_camera.GetComponent<Tracking>();

        m_firstPositionCamera = m_camera.transform.position;
        m_firstPosition = transform.position;

        EventManager.Instance.Loss.AddListener(Falling);
    }

    private void Falling()
    {
        m_jump.enabled = false;
        m_webStart.enabled = false;
        m_collider.isTrigger = true;
        m_tracking.enabled = false;
        m_firstPosition.x = transform.position.x;

        StartCoroutine("FallingCoroutine");
    }

    IEnumerator FallingCoroutine()
    {
        transform.position = Vector3.Lerp(transform.position, m_firstPosition, 50f);
        m_camera.transform.position = Vector3.Lerp(m_camera.transform.position, m_firstPositionCamera, 50f);
        if (transform.position == m_firstPosition)
        {
            Debug.Log("OK");
            m_jump.enabled = true;
            m_webStart.enabled = true;
            m_collider.isTrigger = false;
            m_tracking.enabled = true;
            yield return null;
        }        
    }
}
