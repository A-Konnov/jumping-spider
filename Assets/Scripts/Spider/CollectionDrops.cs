﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectionDrops : MonoBehaviour
{
    private int m_drops = 0;
    public int Drops
    {
        get { return m_drops; }
        set
        {
            m_drops = m_drops + value;
            EventManager.Instance.Drops.Invoke(m_drops);
        }
    }

    private int m_webs = 0;
    public int Webs
    {
        get { return m_webs; }
        set { m_webs = m_webs + value;}
    }
}
