﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WebStart : MonoBehaviour
{
    [SerializeField] LayerMask m_mask;
    [SerializeField] float m_liftingSpeed = 2f;
    [SerializeField] float m_forceJump = 10f;
    [SerializeField] Color m_colorWeb = Color.white;

    private Rigidbody2D m_rb;
    private LineRenderer m_lineRenderer;
    private DistanceJoint2D m_joint;

    private RaycastHit2D m_hit;

    private void Start()
    {
        m_rb = GetComponent<Rigidbody2D>();
        m_lineRenderer = GetComponent<LineRenderer>();
        m_joint = GetComponent<DistanceJoint2D>();
        m_joint.enabled = false;
        m_lineRenderer.startColor = m_colorWeb;
        m_lineRenderer.endColor = m_colorWeb;
        m_lineRenderer.enabled = false;

        EventManager.Instance.Swipe.AddListener(ReleaseWeb);
    }

    private void OnDestroy()
    {
        EventManager.Instance.Swipe.RemoveListener(ReleaseWeb);
    }

    private void Update()
    {
        if (m_joint.enabled)
        {
            m_lineRenderer.SetPosition(0, transform.position);
            m_lineRenderer.SetPosition(1, m_joint.connectedAnchor);

            m_joint.distance = Mathf.Lerp(m_joint.distance, 0, m_liftingSpeed * Time.deltaTime);
            if (m_joint.distance <= 0.1f)
            {
                m_joint.enabled = false;
                m_lineRenderer.enabled = false;

                Vector2 force = Vector2.zero;
                force.y = m_forceJump;
                m_rb.AddForce(force, ForceMode2D.Impulse);
            }
        }
    }

    private void ReleaseWeb(Vector2 direction)
    {
        Debug.DrawRay(transform.position, direction, Color.red, 0.5f);

        m_hit = Physics2D.Raycast(transform.position, direction, float.MaxValue, m_mask);
        if (m_hit.collider != null)
        {
            m_joint.enabled = true;
            m_lineRenderer.enabled = true;

            m_joint.connectedAnchor = m_hit.point;
            m_joint.distance = Vector2.Distance(transform.position, m_hit.point);
        }
    }
}
