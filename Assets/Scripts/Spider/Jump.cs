﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Collider2D))]
public class Jump : MonoBehaviour
{
    [SerializeField] private float m_jumpForce = 5f;
    [SerializeField] private LayerMask m_mask;
    private Rigidbody2D m_rb2D;
    private Collider2D m_collider2D;

    private float m_rayDistance = 0.2f;
    private float m_leftCameraBorder;
    private float m_rightCameraBorder;
    private float m_halfWidth;

    private void Start()
    {
        EventManager.Instance.Touch.AddListener(Jumping);

        m_rb2D = GetComponent<Rigidbody2D>();
        m_collider2D = GetComponent<Collider2D>();

        m_leftCameraBorder = Camera.main.ScreenToWorldPoint(new Vector2(0, 0)).x;
        m_rightCameraBorder = Camera.main.ScreenToWorldPoint(new Vector2(Camera.main.pixelWidth, 0)).x;

        m_halfWidth = m_collider2D.bounds.size.x * 0.5f;
    }

    private void OnDestroy()
    {
        EventManager.Instance.Touch.RemoveListener(Jumping);
    }

    private void Update()
    {
        if (IsBorder() && m_rb2D.velocity != null)
        {
            var velocity = m_rb2D.velocity;
            velocity.x = velocity.x * -1;
            m_rb2D.velocity = velocity;
        }
    }

    private bool IsBorder()
    {
        var leftBound = transform.position.x - m_halfWidth;
        var rightBound = transform.position.x + m_halfWidth;

        if (leftBound <= m_leftCameraBorder || rightBound >= m_rightCameraBorder)
        {
            return true;
        }
        return false;
    }

    private bool IsGrounded()
    {
        var leftBound = m_collider2D.bounds.min.x;
        var rightBound = m_collider2D.bounds.max.x;

        var y = m_collider2D.bounds.min.y;

        var hitLeft = Physics2D.Raycast(new Vector2(leftBound, y), Vector2.down, m_rayDistance, m_mask);
        var hitRight = Physics2D.Raycast(new Vector2(rightBound, y), Vector2.down, m_rayDistance, m_mask);

        if (hitLeft.collider != null || hitRight.collider != null)
        {
            return true;
        }
        return false;
    }

    private void Jumping()
    {
        if (!IsGrounded())
        {
            return;
        }

        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        var force = new Vector2();
        force.x = ray.origin.x - transform.position.x;
        force.y = m_jumpForce;
        m_rb2D.AddForce(force, ForceMode2D.Impulse);
    }
}
