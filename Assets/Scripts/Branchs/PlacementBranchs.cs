﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlacementBranchs : MonoBehaviour
{
    [SerializeField] private Transform[] m_branchPoints;
    [SerializeField] private SOBranchDB m_branchDB;
    private List<GameObject> m_placedBranchs = new List<GameObject>();

    //private void Start()
    //{
    //    PlaceBranchs();
    //}

    public void PlaceBranchs()
    {
        Debug.Log("Placer");
        if (m_placedBranchs.Count != 0)
        {
            RemoveBranchs();
        }

        foreach (var point in m_branchPoints)
        {
            var branch = m_branchDB.GetRandomBranch();

            if (branch == null)
            {
                return;
            }

            branch.transform.position = point.position;
            RandomHeightShift(branch);
            branch.SetActive(true);

            var placeWater = branch.GetComponent<PlacementWater>();
            placeWater.PlaseWater();

            m_placedBranchs.Add(branch);
        }
    }

    private void RandomHeightShift(GameObject branch)
    {
        float randomShift = UnityEngine.Random.Range(-1f, 1f);
        var position = branch.transform.position;
        position.y = position.y + randomShift;
        branch.transform.position = position;
    }

    public void RemoveBranchs()
    {
        foreach (var branch in m_placedBranchs)
        {
            var placeWater = branch.GetComponent<PlacementWater>();
            placeWater.RemoveWater();
            branch.SetActive(false);
        }
        m_placedBranchs.Clear();
    }
}
