﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneratePullBranchs : MonoBehaviour
{
    [SerializeField] private SOBranchDB m_branchDB;

    private void Start()
    {
        m_branchDB.CreatePull();
    }
}
