﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    [SerializeField] private float m_distanceSwipe = 1f;
    private Vector2 m_downPoint;
    private Vector2 m_upPoint;

    private void Update()
    {
        if (Time.timeScale != 0)
        {

            if (Input.GetMouseButtonDown(0))
            {
                m_downPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            }

            if (Input.GetMouseButtonUp(0))
            {
                m_upPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                var direction = m_upPoint - m_downPoint;
                var distance = direction.sqrMagnitude;

                if (distance < (m_distanceSwipe * m_distanceSwipe))
                {
                    EventManager.Instance.Touch.Invoke();
                }
                else
                {
                    EventManager.Instance.Swipe.Invoke(direction);
                }

            }
        }
    }
}
