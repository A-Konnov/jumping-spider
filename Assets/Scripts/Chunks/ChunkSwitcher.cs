﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChunkSwitcher : MonoBehaviour
{
    private List<LevelChunk> m_chunks;
    private GenerateGrid m_generateGrid;
    private int index = 0;

    private void Start()
    {
        m_generateGrid = GetComponent<GenerateGrid>();
        m_chunks = new List<LevelChunk>();

        foreach (Transform chunk in transform)
        {
            var levelChunk = chunk.gameObject.GetComponent<LevelChunk>();
            m_chunks.Add(levelChunk);
        }

        for (int i = 0; i < m_chunks.Count; i++)
        {
            m_chunks[i].Index = i;
            m_chunks[i].OnChunkEnter.AddListener(OnChunkEnter);
            m_chunks[i].gameObject.SetActive(i < 2);
            m_chunks[i].GetComponent<PlacementBranchs>().PlaceBranchs();
        }
    }

    //private void OnChunkEnter(int index)
    //{
    //    var chunkToEnable = index + 2;
    //    var chunkToDisable = index - 1;

    //    if (chunkToEnable < m_chunks.Count)
    //    {
    //        m_chunks[chunkToEnable].GetComponent<PlacementBranchs>().PlaceBranchs();
    //        m_chunks[chunkToEnable].gameObject.SetActive(true);
    //        Debug.Log("Enable and Place" + m_chunks[chunkToEnable].Index);
    //    }
    //    else
    //    {
    //        var newChunk = m_generateGrid.AddChunk(m_chunks[chunkToEnable - 1].transform.position);
    //        newChunk.Index = chunkToEnable;
    //        newChunk.GetComponent<PlacementBranchs>().PlaceBranchs();
    //        newChunk.OnChunkEnter.AddListener(OnChunkEnter);
    //        m_chunks.Add(newChunk);
    //        Debug.Log("Create and Place" + m_chunks[chunkToEnable].Index);
    //    }

    //    if (chunkToDisable >= 0)
    //    {
    //        m_chunks[chunkToDisable].GetComponent<PlacementBranchs>().RemoveBranchs();
    //        m_chunks[chunkToDisable].gameObject.SetActive(false);
    //    }
    //}

    //ver 3
    private void OnChunkEnter()
    {
        var newChunk = m_generateGrid.AddChunk(m_chunks[m_chunks.Count - 1].transform.position);
        newChunk.GetComponent<PlacementBranchs>().PlaceBranchs();
        newChunk.OnChunkEnter.AddListener(OnChunkEnter);
        newChunk.gameObject.name = "Chunk " + index;
        index++;
        m_chunks.Add(newChunk);

        Debug.Log("chunks " + m_chunks.Count);

        if (m_chunks.Count > 3)
        {
            Debug.Log("Destroy " + m_chunks[0].gameObject.name);
            m_chunks[0].GetComponent<PlacementBranchs>().RemoveBranchs();
            m_chunks[0].gameObject.SetActive(false);
            m_chunks.Remove(m_chunks[0]);
        }

        //var chunkToEnable = index + 2;
        //var chunkToDisable = index - 1;

        //var newChunk = m_generateGrid.AddChunk(m_chunks[chunkToEnable - 1].transform.position);
        //newChunk.Index = chunkToEnable;
        //newChunk.GetComponent<PlacementBranchs>().PlaceBranchs();
        //newChunk.OnChunkEnter.AddListener(OnChunkEnter);
        //m_chunks.Add(newChunk);
        //Debug.Log("Create and Place" + m_chunks[chunkToEnable].Index);

        //if (chunkToDisable >= 0)
        //{
        //    m_chunks[chunkToDisable].GetComponent<PlacementBranchs>().RemoveBranchs();
        //    m_chunks[chunkToDisable].gameObject.SetActive(false);
        //}
    }

    //ver 2
    //private void OnChunkEnter(int index)
    //{
    //    var chunkToEnable = index + 2;
    //    var chunkToDisable = index - 1;

    //    var newChunk = m_generateGrid.AddChunk(m_chunks[chunkToEnable - 1].transform.position);
    //    newChunk.Index = chunkToEnable;
    //    newChunk.GetComponent<PlacementBranchs>().PlaceBranchs();
    //    newChunk.OnChunkEnter.AddListener(OnChunkEnter);
    //    m_chunks.Add(newChunk);
    //    Debug.Log("Create and Place" + m_chunks[chunkToEnable].Index);

    //    if (chunkToDisable >= 0)
    //    {
    //        m_chunks[chunkToDisable].GetComponent<PlacementBranchs>().RemoveBranchs();
    //        m_chunks[chunkToDisable].gameObject.SetActive(false);
    //    }
    //}

    //private void OnChunkExit(int index)
    //{
    //    var chunkToEnable = index + 2;
    //    var chunkToDisable = index - 1;

    //    if (chunkToEnable < m_chunks.Count)
    //    {
    //        m_chunks[chunkToEnable].gameObject.SetActive(true);

    //        m_chunks[chunkToEnable].GetComponent<PlacementBranchs>().PlaceBranchs();        
    //    }
    //    else
    //    {
    //        var newChunk = m_generateGrid.AddChunk(m_chunks[chunkToEnable - 1].transform.position);
    //        newChunk.Index = chunkToEnable;
    //        newChunk.GetComponent<PlacementBranchs>().PlaceBranchs();
    //        newChunk.OnChunkEnter.AddListener(OnChunkEnter);
    //        newChunk.OnChunkExit.AddListener(OnChunkExit);
    //        m_chunks.Add(newChunk);
    //    }

    //    if (chunkToDisable >= 0)
    //    {
    //        m_chunks[chunkToDisable].gameObject.SetActive(false);
    //        m_chunks[chunkToDisable].GetComponent<PlacementBranchs>().RemoveBranchs();
    //    }
    //}

    private void OnDestroy()
    {
        foreach (var chunk in m_chunks)
        {
            chunk.OnChunkEnter.RemoveListener(OnChunkEnter);
            //chunk.OnChunkExit.RemoveListener(OnChunkExit);
        }
    }

}
