﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class GenerateGrid : MonoBehaviour
{
    [SerializeField] private SOChunkBD m_chunkBD;

    [ContextMenu("Clear")]
    private void Clear()
    {
        while (transform.childCount > 0)
        {
            DestroyImmediate(transform.GetChild(0).gameObject);
        }
    }

    private void Awake()
    {
        Generate();
    }

    [ContextMenu("Generate")]
    public void Generate()
    {
        Clear();
        m_chunkBD.GeneratePull();

        var pos = Vector3.zero;

        var startChunk = m_chunkBD.GetStartChunk();
        startChunk.transform.parent = gameObject.transform;
        startChunk.transform.position = pos;

        for (int i = 0; i< m_chunkBD.GetCountPull; i++)
        {
            var chunk = m_chunkBD.GetChunk();
            chunk.transform.parent = gameObject.transform;

            var offset = Vector3.up * chunk.Height;
            pos += offset;
            chunk.transform.position = pos;
        }
    }

    public LevelChunk AddChunk(Vector3 pos)
    {
        var chunk = m_chunkBD.GetChunk();
        chunk.transform.parent = gameObject.transform;

        var offset = Vector3.up * chunk.Height;
        pos += offset;
        chunk.transform.position = pos;
        chunk.gameObject.SetActive(true);
        chunk.IsEnterColliderFalse();

        return chunk;
    }
}


