﻿using System;
using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEngine.Events;

//[Serializable]
//public class UnityIntEvent : UnityEvent<int> { }

[RequireComponent(typeof(Tilemap))]
[RequireComponent(typeof(BoxCollider2D))]

//[ExecuteInEditMode]
public class LevelChunk : MonoBehaviour
{
    [SerializeField] private int m_index;

    public int Index { get { return m_index; } set { m_index = value; } }

    private Tilemap m_map;
    private BoxCollider2D m_trigger;
    private bool m_isEnterCollider = false;

    public UnityEvent OnChunkEnter = new UnityEvent();
    //public UnityIntEvent OnChunkEnter = new UnityIntEvent();
    //public UnityIntEvent OnChunkExit = new UnityIntEvent();

    public int Height { get { return Map.size.y; } } //высота
    public int Width { get { return Map.size.x; } } //ширина
    public float OffsetY { get { return Map.cellBounds.center.y; } } //смещение центра
    public float OffsetX { get { return Map.cellBounds.center.x; } }

    private Tilemap Map
    {
        get
        {
            if (m_map == null)
            {
                m_map = GetComponent<Tilemap>();
            }

            return m_map;
        }
    }

    private void Start()
    {
        m_map = GetComponent<Tilemap>();
        m_trigger = GetComponent<BoxCollider2D>();
        m_trigger.isTrigger = true;

        SetTriggerSizeAndOffset();
    }

    //устанавливать размер и смещение
    private void SetTriggerSizeAndOffset()
    {
        var xOffset = m_map.cellBounds.center.x;
        var yOffset = m_map.size.y * 0.5f + m_map.cellBounds.center.y - 0.5f;
        m_trigger.offset = new Vector2(xOffset, yOffset);
        m_trigger.size = new Vector2(m_map.size.x, 1);
    }

    //private void OnTriggerExit2D(Collider2D collision)
    //{
    //    var spider = collision.GetComponent<Jump>();
    //    if (spider == null) return;

    //    var centerY = transform.position.y + m_trigger.offset.y; //центр триггера в мировых координатах
    //    if (collision.transform.position.y > centerY)
    //    {
    //        OnChunkExit.Invoke(Index);
    //    }
    //    else if (collision.transform.position.x < centerY)
    //    {
    //        OnChunkEnter.Invoke(Index);
    //    }

    //}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!m_isEnterCollider)
        {
            m_isEnterCollider = true;
            OnChunkEnter.Invoke();
        }
    }

    public void IsEnterColliderFalse()
    {
        m_isEnterCollider = false;
    }
}


