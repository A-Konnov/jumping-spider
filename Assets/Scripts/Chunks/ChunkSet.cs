﻿using UnityEngine;
using Random = UnityEngine.Random;

[CreateAssetMenu]
public class ChunkSet : ScriptableObject
{
    public enum RepeatType
    {
        Repeat,
        Random
    }

    [Header("Property Chunk")]
    [SerializeField] [Range(2, 100)] private int m_size = 2; //размер уровня, Range - ограничивает ввод
    [SerializeField] private RepeatType m_repeat = RepeatType.Repeat;
    [SerializeField] private LevelChunk m_firstChunk;
    [SerializeField] private LevelChunk m_lastChunk;
    [SerializeField] private LevelChunk[] m_chunks;

    [Header("Tuning Drops")]
    [SerializeField] private GameObject m_dropPrefab;
    public GameObject Drop { get { return m_dropPrefab; } }

    [SerializeField] private int m_dropAmount;
    public int DropAmount { get { return m_dropAmount; } }

    [SerializeField] private LayerMask m_setHigherForDrops;
    public LayerMask SetHigherForDrops { get { return m_setHigherForDrops; } }

    [Header("Tuning Webs")]
    [SerializeField] private GameObject m_webPrefab;
    public GameObject Web { get { return m_webPrefab; } }

    [SerializeField] private int m_webAmount;
    public int WebAmount { get { return m_webAmount; } }

    [SerializeField] private LayerMask m_setHigherForWebs;
    public LayerMask SetHigherForWebs { get { return m_setHigherForWebs; } }




    public int Size { get { return m_size; } }

    public LevelChunk GetNextChunk(int i)
    {
        LevelChunk result = null;

        if (i == 0)
        {
            result = m_firstChunk;
        }
        else if (i == Size - 1)
        {
            result = m_lastChunk;
        }
        else
        {
            if (m_repeat == RepeatType.Random)
            {
                result = GetRndChunk();
            }
            else if (m_repeat == RepeatType.Repeat)
            {
                result = GetRepeatChunk(i);
            }
        }


        return result;
    }


    private LevelChunk GetRndChunk()
    {
        var rndIndex = Random.Range(0, m_chunks.Length);
        return m_chunks[rndIndex];
    }

    private LevelChunk GetRepeatChunk(int i)
    {
        i = (int)Mathf.Repeat(i - 1, m_chunks.Length);
        return m_chunks[i];
    }
}
