﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LossTrigger : MonoBehaviour
{
    [SerializeField] private bool m_enable;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!m_enable) { return; }
        var player = collision.GetComponent<Jump>();
        if (player != null)
        {
            Debug.Log("Loss!");
            EventManager.Instance.Loss.Invoke();
        }
    }
}
