﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class Tracking : MonoBehaviour
{
    [Header("Reference:")]
    [SerializeField] private Grid m_grid;
    [SerializeField] private Transform m_spider;

    [Header("Properties:")]
    [SerializeField] private float m_biasCamera = 1f;

    private int m_totalHeight;
    private float m_bottomBorderCamera;
    private float m_topBorderCamera;

    private void Start()
    {
        ScaleCamera();
        BorderCamera();
        StartPositionCamera();
    }

    private void StartPositionCamera()
    {
        //new position to camera
        var newPosition = transform.position;
        newPosition.x = m_grid.transform.position.x;
        newPosition.y = m_bottomBorderCamera;
        transform.position = newPosition;
    }

    private void BorderCamera()
    {
        //border to camera
        var tileMax = m_grid.transform.GetChild(m_grid.transform.childCount - 1).GetComponent<Tilemap>();
        var tileMin = m_grid.transform.GetChild(0).GetComponent<Tilemap>();
        var yMaxLocal = tileMax.cellBounds.yMax;
        var yMinLocal = tileMin.cellBounds.yMin;
        Vector3 yMax = tileMax.CellToWorld(new Vector3Int(0, yMaxLocal, 0));
        Vector3 yMin = tileMin.CellToWorld(new Vector3Int(0, yMinLocal, 0));
        var yMaxGlobal = yMax.y;
        var yMinGlobal = yMin.y;

        m_topBorderCamera = yMaxGlobal - Camera.main.orthographicSize;
        m_bottomBorderCamera = yMinGlobal + Camera.main.orthographicSize;
    }

    private void ScaleCamera()
    {
        //new scale camera
        var width = m_grid.GetComponentInChildren<LevelChunk>().Width;
        Camera.main.orthographicSize = width / (Camera.main.aspect * 2);
    }

    private void Update()
    {
        var newPositionY = m_spider.position.y + m_biasCamera;

        if (newPositionY < transform.position.y)
        {
            return;
        }

        newPositionY = Mathf.Lerp(transform.position.y, newPositionY, Time.deltaTime);
        var pos = transform.position;
        pos.y = Mathf.Clamp(newPositionY, m_bottomBorderCamera, float.MaxValue);
        transform.position = pos;
    }

}
