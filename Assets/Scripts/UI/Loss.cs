﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class Loss : MonoBehaviour
{
    [SerializeField] private CollectionDrops m_drops;
    [SerializeField] private TextMeshProUGUI m_score;
    [SerializeField] private TextMeshProUGUI m_bestScore;

    private void Start()
    {
        EventManager.Instance.Loss.AddListener(LossOpen);
    }

    private void OnDestroy()
    {
        EventManager.Instance.Loss.RemoveListener(LossOpen);
    }

    private void LossOpen()
    {
        var bestScore = PlayerPrefs.GetInt("Score");
        if (m_drops.Drops > bestScore)
        {
            PlayerPrefs.SetInt("Score", m_drops.Drops);
            bestScore = m_drops.Drops;
        }

        m_score.text = string.Format("score: {0}", m_drops.Drops);
        m_bestScore.text = string.Format("best score: {0}", bestScore);

        var animator = GetComponent<Animator>();
        animator.SetTrigger("Open");
        Time.timeScale = 0;
    }

    public void Restart()
    {
        Scene scene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(scene.name);
    }
}
