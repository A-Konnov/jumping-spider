﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Points : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI m_points;

    private void Start()
    {
        EventManager.Instance.Drops.AddListener(ShowDrops);
    }

    private void OnDestroy()
    {
        EventManager.Instance.Drops.RemoveListener(ShowDrops);
    }

    private void ShowDrops(int drops)
    {
        m_points.text = string.Format("{0}", drops);
    }
}
