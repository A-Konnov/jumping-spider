﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Start : MonoBehaviour
{
    [SerializeField] private Animator m_UIPoints;

    private void Awake()
    {
        Time.timeScale = 0;
    }

    public void OnClickStart()
    {
        Time.timeScale = 1;
        var animator = GetComponent<Animator>();
        animator.SetTrigger("Close");
        m_UIPoints.SetTrigger("Open");
    }
}
