﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Web : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        var collectionDrops = collision.GetComponent<CollectionDrops>();
        if (collectionDrops != null)
        {
            collectionDrops.Webs = 1;
        }
        Destroy(gameObject);
    }
}
