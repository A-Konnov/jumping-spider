﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System;

[Serializable]
public class SwipeEvent : UnityEvent<Vector2> { }
public class DropsEvent : UnityEvent<int> { }

public class EventManager : MonoBehaviour
{
    public static EventManager Instance { get; private set; }

    public UnityEvent Touch = new UnityEvent();
    public UnityEvent Loss = new UnityEvent();
    public SwipeEvent Swipe = new SwipeEvent();
    public DropsEvent Drops = new DropsEvent();

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }
    }
}