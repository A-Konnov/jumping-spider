﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneratePullWater : MonoBehaviour
{
    [SerializeField] private SOWaterDB m_waterDB;

    private void Start()
    {
        m_waterDB.CreatePull();
    }
}