﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Water : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        var collectionDrops = collision.GetComponent<CollectionDrops>();
        if (collectionDrops != null)
        {
            collectionDrops.Drops = 1;
        }
        gameObject.SetActive(false);
    }
}
