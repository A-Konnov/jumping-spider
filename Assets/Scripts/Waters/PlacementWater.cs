﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using Random = UnityEngine.Random;

public class PlacementWater : MonoBehaviour
{
    [SerializeField] private SOWaterDB m_waterDB;
    private List<GameObject> m_placeWater = new List<GameObject>();

    private RaycastHit2D m_hit;
    private Collider2D m_collider;

    private void Awake()
    {
        m_collider = GetComponent<Collider2D>();
    }

    public void RemoveWater()
    {
        if (m_placeWater.Count != 0)
        {
            foreach (var water in m_placeWater)
            {
                water.SetActive(false);
            }
            m_placeWater.Clear();
        }
    }

    public void PlaseWater()
    {
        RemoveWater();

        float leftBounds =  m_collider.bounds.center.x - (m_collider.bounds.size.x * 0.5f) + 0.1f;
        float rightBounds = m_collider.bounds.center.x + (m_collider.bounds.size.x * 0.5f) - 0.1f;
        float positionY = m_collider.bounds.center.y + 1f;
        float positionX = Random.Range(leftBounds, rightBounds);
        Vector2 origin;
        origin.x = positionX;
        origin.y = positionY;

        m_hit = Physics2D.Raycast(origin, Vector2.down);
        if (m_hit.collider == null)
        {
            return;
        }

        var water = m_waterDB.GetWater();
        m_placeWater.Add(water);
        origin.y = m_hit.point.y + 0.2f;
        water.transform.position = origin;
    }
}
